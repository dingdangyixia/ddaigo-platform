package com.purchase.rest;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.purchase.service.StoreService;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

@Controller
@RequestMapping("/store")
public class StoreController {
	
	@Resource
	StoreService storeService;
	
	
	/**
	 * 获取店铺列表
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/storeList", method = RequestMethod.POST)
	public ResponseForm getStoreList(@RequestBody RequestForm param) {
		return storeService.getStoreList(param);
	}
	
	
	/**
	 * 获取店铺列表
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getStores", method = RequestMethod.POST)
	public ResponseForm getStores(@RequestBody RequestForm param) {
		return storeService.getStores(param);
	}
	
	
	/**
	 * 根据storeNo查询门店信息
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getStoreByStroreNo", method = RequestMethod.POST)
	public ResponseForm getStoreByStroreNo(@RequestBody RequestForm param) {
		return storeService.getStoreByStroreNo(param);
	}
	
	
	/**
	 * 根据ID查询门店信息
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getStoreById", method = RequestMethod.POST)
	public ResponseForm getStoreById(@RequestBody RequestForm param) {
		return storeService.getStoreById(param);
	}
	
	
	/**
	 * 新增门店
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/addStore", method = RequestMethod.POST)
	public ResponseForm addStore(@RequestBody RequestForm param) {
		return storeService.addStore(param);
	}
	
	/**
	 * 修改门店
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/editStore", method = RequestMethod.POST)
	public ResponseForm editStore(@RequestBody RequestForm param) {
		return storeService.editStore(param);
	}
	
	/**
	 * 新增门店时，门店品牌信息
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getBrand", method = RequestMethod.POST)
	public ResponseForm getBrand(@RequestBody RequestForm param) {
		return storeService.getBrand(param);
	}

}
