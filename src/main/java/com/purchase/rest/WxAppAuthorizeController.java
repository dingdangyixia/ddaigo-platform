package com.purchase.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.purchase.service.WxAppAuthorizeService;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

/**
 * @ClassName: WxAppAuthorizeController.java
 * @Description: app用户微信授权请求控制器模块
 * @Device: Android/Ios端
 * @author: liuhoujie
 * @date: 2018年6月21日
 */
@Controller
@RequestMapping("/authorization")
public class WxAppAuthorizeController {
	
	@Autowired
	WxAppAuthorizeService authorizeService;
	
	
	/**
	 * @DESC APP端授权
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/app2authorize", method = RequestMethod.POST)
	public ResponseForm app2authorize(@RequestBody RequestForm param) {
		return authorizeService.app2authorize(param);
	}
	
	
	/**
	 * 获取用户信息
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getUserInfo", method = RequestMethod.POST)
	public ResponseForm getUserInfo(@RequestBody RequestForm param) {
		return authorizeService.getUserInfo(param);
	}
}
