package com.purchase.rest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.purchase.service.AreaService;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

@Controller
@RequestMapping("/area")
public class AreaController {
	
	@Resource
	AreaService areaService;
	
//	@ResponseBody
//	@RequestMapping(value = "/getAllProvince", method = RequestMethod.GET)
//	public ResponseForm getAllProvince() {
//		//Map<String, Object> paramMap = (Map<String, Object>) param.getData();
//		return areaService.getAllProvince();
//	}
	
	/**
	 * 获取地域信息
	 * @param ResponseForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getAreaByPid", method = RequestMethod.POST)
	public ResponseForm getAllCityByPid(@RequestBody RequestForm param,HttpServletRequest request) {
		return areaService.getAreaByPid(param,request);
	}

}
