package com.purchase.rest;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.purchase.service.ForwardService;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

/**
 * @ClassName: ForwardController.java
 * @Description: 转发相关接口
 * @author: liuhoujie
 * @date: 2018年5月22日
 */
@Controller
@RequestMapping("/forward")
public class ForwardController {
	
	@Resource
	ForwardService forwardService;
	
	@ResponseBody
	@RequestMapping(value="saveForward",method = RequestMethod.POST)
	private ResponseForm saveForward(@RequestBody RequestForm param) {
		return forwardService.saveForward(param);
	}
}
