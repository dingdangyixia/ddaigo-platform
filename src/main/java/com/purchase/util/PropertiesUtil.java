package com.purchase.util;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Properties;


/**
 * @ClassName: PropertiesUtil.java
 * @Description: 从.properties配置文件中获取值
 * @author: liuhoujie
 * @date: 2018年1月15日
 */
public class PropertiesUtil {
	//获取当前路径
	private static String sysPath = System.getProperty("user.dir");
	//conf.properties路径
	public static String CONFPATH = "/META-INF/conf.properties";
	public static String WXPATH = "/META-INF/wxpay.properties";

	/**
	 * @Description: 通过key获取value
	 * @param key
	 * @return
	 */
	public static String getValue(String filePath, String key) {
		Properties prop = new Properties();
		InputStream inS = null;
		String value = null;
		try {
			// 读取属性文件 conf.properties 得到输入流
			inS = PropertiesUtil.class.getResourceAsStream(filePath);
			if (inS == null) {
				inS = new BufferedInputStream(new FileInputStream(filePath));
			}
			prop.load(inS);
			Iterator<String> it = prop.stringPropertyNames().iterator();
			while (it.hasNext()) {
				String k = it.next();
				if (k.equalsIgnoreCase(key)) {
					value = prop.getProperty(key);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value.trim();
	}
	
	
}
