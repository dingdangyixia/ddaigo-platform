package com.purchase.util;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * httpClient工具类
 */
public class HttpClient {

    private static Logger logger = LoggerFactory.getLogger(HttpClient.class);
    /**
     * 接收http请求处理
     * @param request
     * @param content 内容-谁发的通知
     * @return
     */
    public static String reciveHttpReq(HttpServletRequest request, String content) throws Exception {

        String resMes = "";
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream(), "GBK"));
            String line;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            resMes = sb.toString();
            logger.info(content+"[----->]" + resMes);
        } catch (IOException e) {
            e.printStackTrace();
            throw new Exception("接收http请求处理异常");
        }
        return resMes;
    }

}
