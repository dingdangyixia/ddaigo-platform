package com.purchase.util.wxpay.app;

import com.purchase.util.PropertiesUtil;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public class WXPayConfigImplAPP extends WXPayConfigAPP {

    private byte[] certData;
    private static WXPayConfigImplAPP INSTANCE;

    private WXPayConfigImplAPP() throws Exception{
        String certPath = PropertiesUtil.getValue(PropertiesUtil.WXPATH,"wxpay.certPath.APP");
        File file = new File(certPath);
        InputStream certStream = new FileInputStream(file);
        this.certData = new byte[(int) file.length()];
        certStream.read(this.certData);
        certStream.close();
    }

    public static WXPayConfigImplAPP getInstance() throws Exception{
        if (INSTANCE == null) {
            synchronized (WXPayConfigImplAPP.class) {
                if (INSTANCE == null) {
                    INSTANCE = new WXPayConfigImplAPP();
                }
            }
        }
        return INSTANCE;
    }

    public String getAppID() {
        return PropertiesUtil.getValue(PropertiesUtil.WXPATH,"wxpay.appid.APP");
    }

    public String getMchID() {
        return PropertiesUtil.getValue(PropertiesUtil.WXPATH,"wxpay.mchid.APP");
    }

    public String getKey() {
        return PropertiesUtil.getValue(PropertiesUtil.WXPATH,"wxpay.key.APP");
    }

    public InputStream getCertStream() {
        ByteArrayInputStream certBis;
        certBis = new ByteArrayInputStream(this.certData);
        return certBis;
    }


    public int getHttpConnectTimeoutMs() {
        return 2000;
    }

    public int getHttpReadTimeoutMs() {
        return 10000;
    }

    IWXPayDomainAPP getWXPayDomain() {
        return WXPayDomainSimpleImplAPP.instance();
    }

    public String getPrimaryDomain() {
        return "api.mch.weixin.qq.com";
    }

    public String getAlternateDomain() {
        return "api2.mch.weixin.qq.com";
    }

    @Override
    public int getReportWorkerNum() {
        return 1;
    }

    @Override
    public int getReportBatchSize() {
        return 2;
    }
}
