package com.purchase.util.wxpay;

import java.math.BigDecimal;

/**
 * 金额转换工具类
 */
public class TransAmountUtil {

    /**
     * 元转分（微信使用）
     * @param amount
     * @return
     */
    public static int yuanToFen(BigDecimal amount){
        return amount.multiply(new BigDecimal(100)).intValue();
    }

    /**
     * 分转元（微信使用）
     * @param amount
     * @return
     */
    public static BigDecimal fenToYuan(String amount){
        return new BigDecimal(amount).divide(new BigDecimal(100));
    }
}
