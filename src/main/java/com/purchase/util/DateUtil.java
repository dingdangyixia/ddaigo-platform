package com.purchase.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/**
 * @ClassName: DateUtil.java
 * @Description: 时间工具
 * @author: liuhoujie
 * @date: 2018年3月27日
 */
public class DateUtil {

	public static SimpleDateFormat format_ymd_hms = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static SimpleDateFormat format_ymd = new SimpleDateFormat("yyyy-MM-dd");

	public static String getNow() {
		return format_ymd_hms.format(new Date());
	}

	public static String getToday_Ymd() {
		return format_ymd.format(new Date());
	}

	public static String getFirstDayOfThisWeek_Ymd() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		int d = 0;
		if (cal.get(Calendar.DAY_OF_WEEK) == 1) {
			d = -6;
		} else {
			d = 2 - cal.get(Calendar.DAY_OF_WEEK);
		}
		cal.add(Calendar.DAY_OF_WEEK, d);
		return format_ymd.format(cal.getTime());
	}

	public static String getLastDayOfThisWeek_Ymd() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		int d = 0;
		if (cal.get(Calendar.DAY_OF_WEEK) == 1) {
			d = -6;
		} else {
			d = 2 - cal.get(Calendar.DAY_OF_WEEK);
		}
		cal.add(Calendar.DAY_OF_WEEK, d);
		cal.add(Calendar.DAY_OF_WEEK, 6);
		return format_ymd.format(cal.getTime());
	}

	public static String getUUID() {
		return UUID.randomUUID().toString();
	}


	/**
	 * @DESC 时间戳转时间date格式 
	 * @param objectDate
	 * @return
	 */
	public static Date toDate(Object objectDate) {
		try {
			String strDate = format_ymd_hms.format(objectDate);
			Date date = format_ymd_hms.parse(strDate);
			return date;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public static Date toDate(Long longDate) {
		try {
			String strDate = format_ymd_hms.format(longDate);
			Date date = format_ymd_hms.parse(strDate);
			return date;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static String printStr(Object objectDate) {
		try {
			String strDate = format_ymd_hms.format(objectDate);
			return strDate;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	public static void main(String[] args) {
//		Object currentTimeMillis = System.currentTimeMillis();
//		System.out.println(currentTimeMillis);
//		Date date = DateUtil.toDate(currentTimeMillis);
//		Date dayChange = CalculateUtil.dayChange(date, -90);
//		Calendar calendar = Calendar.getInstance();
//		calendar.setTime(dayChange);
//		long timeInMillis = calendar.getTimeInMillis();
//		System.out.println(timeInMillis);
//		
//		Date date = DateUtil.printStr(1525795200);
//		System.out.println(date);
	}
}
