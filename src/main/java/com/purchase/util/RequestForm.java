package com.purchase.util;

import java.io.Serializable;

public class RequestForm implements Serializable{

	private static final long serialVersionUID = 1L;

	private String channel; //来源渠道 0pc 1安卓 2苹果
	private String userId;  //用户ID
	private String storeId; //门店ID
	private Object data;    //请求内容
	
	public Object getData(){
		return this.data;
	}
	public void setData(Object data){
		this.data = data;
	}
	
	public String getUserId(){
		return this.userId;
	}
	public void setUserId(String userId){
		this.userId = userId;
	}
	
	public String getChannel(){
		return this.channel;
	}
	public void setChannel(String channel){
		this.channel = channel;
	}
	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

}
