package com.purchase.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.ddyx.service.IALISMSService;
import com.ddyx.service.IMobiePushService;
import com.ddyx.service.impl.ALISMSServiceService;
import com.ddyx.service.impl.AliyunMobilePushService;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @ClassName: AliIM.java
 * @Description: aliyun 短信
 * @author: liuhoujie
 * @date: 2018年4月12日
 */
public class AliIM {
	private static ObjectMapper jsonMapper = new ObjectMapper();
	private static String aliTemplateCode = PropertiesUtil.getValue(PropertiesUtil.CONFPATH, "aliTemplateCode");
	private static String aliSignName = PropertiesUtil.getValue(PropertiesUtil.CONFPATH, "aliSignName");
	// private static String title = "DING_CHAT";

	/**
	 * @dsec 消息推送
	 * @param mobile
	 * @param message
	 */
	// public static void push2ali(String mobile,String message) {
	// /*true为正式环境 false为测试环境*/
	// IMobiePushService mobiePushService=new AliyunMobilePushService(true);
	// /*根据Account(电话)发送消息*/
	// mobiePushService.pushMessageByAccount(mobile,title, message);
	// }

	/**
	 * @Description 短信验证码
	 * @param mobile
	 * @param message
	 * @return
	 */
	public static Boolean shortMessage(String phone, String validateCode) {
		try {
			IALISMSService SMSService = new ALISMSServiceService();
			SendSmsRequest request = new SendSmsRequest();

			request.setSignName(aliSignName);
			Map map = new HashMap<>();

			map.put("code", validateCode);//

			String json = jsonMapper.writeValueAsString(map);
			request.setTemplateCode(aliTemplateCode);

			request.setPhoneNumbers(phone);//

			request.setTemplateParam(json);
			return SMSService.sendSMS(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @Description 数字随机验证码 (默认4位,最大8位)
	 * @return
	 */
	public static String getVerifyCode(Integer bit) {
		String[] sources = { "1", "0", "2", "4", "8", "3", "5", "9", "7", "6" };
		String verifyCode = "";
		if (bit == null && bit < 4 && bit > 8) {
			bit = 4;
		}
		for (int i = 0; i < bit; i++) {
			verifyCode += sources[new Random().nextInt(sources.length)];
		}
		return verifyCode;
	}

	public static void main(String[] args) {
		 shortMessage("13269621006", getVerifyCode(6));
	}
}
