package com.purchase.mapper;

import com.purchase.entity.AppVersion;

import tk.mybatis.mapper.common.Mapper;

@MyBatisRepository
public interface AppVersionMapper extends Mapper<AppVersion>{

	AppVersion getLatestVer(String appAlias);
	
}
