package com.purchase.mapper;

import com.purchase.entity.UserAddress;

import tk.mybatis.mapper.common.Mapper;

@MyBatisRepository
public interface UserAddressMapper extends Mapper<UserAddress>  {

	
	void updateDefaultOptions(String wxUid);
	
}
