package com.purchase.mapper;

import com.purchase.entity.RefundPay;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

@MyBatisRepository
public interface RefundPayMapper extends Mapper<RefundPay> {

    /**
     * 保存退款流水
     *
     * @param paramMap
     * @return
     */
    int saveRefundPayOrder(Map<String, Object> paramMap);

    /**
     * 修改退款流水
     *
     * @param paramMap
     * @return
     */
    int modifyRefundPayOrder(Map<String, Object> paramMap);

    /**
     * 根据 orderId 获取退款流水
     *
     * @param orderId
     * @return
     */
    Map<String, Object> getRefundPayOrderByOrderId(@Param("orderId") String orderId);

    /**
     * 根据 id 获取退款流水
     *
     * @param id
     * @return
     */
    Map<String, Object> getRefundPayOrderById(@Param("id") String id);

}
