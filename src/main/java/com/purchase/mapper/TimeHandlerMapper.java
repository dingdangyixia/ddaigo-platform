package com.purchase.mapper;

@MyBatisRepository
public interface TimeHandlerMapper {
	 /**
     * 订单状态切换
     * 优惠券信息自动更换
     */
    void autoSwitchOrderStatus();
    
    /**
     * 订单完成状态(已完成/确认收货)
     */
    void receivedOrdersStatus();
    
	/**
	 * 定期删除失效购物车单
	 */
	void cleanInvalidCarts();
	
	/**
	 * 定时开启活动
	 */
	void autoSwitchActivitiesStatus();
}
