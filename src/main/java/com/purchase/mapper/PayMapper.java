package com.purchase.mapper;

import com.purchase.entity.PayOrder;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

@MyBatisRepository
public interface PayMapper extends Mapper<PayOrder> {

    /**
     * 保存流水
     *
     * @param paramMap
     * @return
     */
    int savePayOrder(Map<String, Object> paramMap);

    /**
     * 修改流水
     *
     * @param payOrderMap
     * @return
     */
    int modifyPayOrder(Map<String, Object> payOrderMap);

    /**
     * 修改订单优惠券时修改流水金额
     * @param payOrderMap
     * @return
     */
    int modifyPayOrderCouponPrice(Map<String, Object> payOrderMap);

    /**
     * 根据id获取流水
     *
     * @param payOrderMap
     * @return
     */
    Map<String, Object> getPayOrderById(Map<String, Object> payOrderMap);

    /**
     * 根据 orderId 获取流水
     *
     * @param orderId
     * @return
     */
    Map<String, Object> getUPayOrderByOrderId(@Param("orderId") String orderId);

    /**
     * 根据 orderId 获取流水
     *
     * @param orderId
     * @return
     */
    Map<String, Object> getRefundablePayOrderByOrderId(@Param("orderId") String orderId);

    /**
     * 计数流水
     *
     * @param orderId
     */
    int getCountPayOrder(@Param("orderId") String orderId);

    /**
     * 根据 orderId 获取流水
     * @param orderId
     * @return
     */
    Map<String, Object> getPayOrderByOrderId(@Param("orderId") String orderId);

    int delPayOrderByOrderId(@Param("orderId") String orderId);

}
