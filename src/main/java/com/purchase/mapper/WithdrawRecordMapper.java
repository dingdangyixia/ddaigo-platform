package com.purchase.mapper;

import com.purchase.entity.WithdrawRecord;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

@MyBatisRepository
public interface WithdrawRecordMapper extends Mapper<WithdrawRecord> {

    int insertOne(Map<String, Object> paramMap);

    List<WithdrawRecord> selectList(@Param("wxUid") String wxUid);

    int updateById(Map<String, Object> paramMap);

    WithdrawRecord selectById(@Param("id") Integer id);

}
