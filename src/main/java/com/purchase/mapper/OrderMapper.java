package com.purchase.mapper;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.purchase.entity.Order;
import com.purchase.entity.OrderDetail;

import tk.mybatis.mapper.common.Mapper;

@MyBatisRepository
public interface OrderMapper extends Mapper<Order> {

    /**
     * 根据订单状态、物流状态获取订单list
     *
     * @param paramMap
     * @return
     */
    List<Map<String, Object>> getOrderByStatus(Map<String, Object> paramMap);

    /**
     * 根据订单id获取订单详情列表
     *
     * @param orderId
     * @return
     */
    List<Map<String, Object>> getOrderDetailListByOrderId(String orderId);

    /**
     * 根据id获取订单详情
     *
     * @param orderDetailId
     * @return
     */
    Map<String, Object> getOrderDetailById(String orderDetailId);

    /**
     * 修改订单是否删除
     *
     * @param paramMap
     * @return
     */
    int updateOrderIsDeleteByOrderId(Map<String, Object> paramMap);

    /**
     * 生成订单详情
     *
     * @param paramMap
     */
    int createOrderDetails(List<Map<String, Object>> paramMap);

    /**
     * 生成订单
     *
     * @param paramMap
     * @return
     */
    int createOrder(Map<String, Object> paramMap);

    /**
     * 根据id获取订单
     *
     * @param orderId
     * @return
     */
    Map<String, Object> getOrderByIdAndStoreNo(@Param("orderId") String orderId, @Param("storeNo") String storeNo);

    /**
     * 根据id获取订单
     *
     * @param orderId
     * @return
     */
    Map<String, Object> getWaitPayOrderPriceByIdForPay(@Param("orderId") String orderId);

    /**
     * 根据wxUid获取地址id和门店运费
     *
     * @param wxUid
     * @return
     */
    Map<String, Object> getUserAddressIdAndPostageByWxUid(@Param("wxUid") String wxUid, @Param("storeNo") String storeNo);

    /**
     * 修改订单关联地址
     *
     * @param paramMap
     * @return
     */
    int updateOrderAddress(Map<String, Object> paramMap);

    /**
     * 根据用户id获取不同状态订单的数量
     *
     * @param wxUid
     * @return
     */
    Map<String, Object> getOrderCountByWxUid(@Param("wxUid") String wxUid);

    /**
     * 修改订单状态
     *
     * @return
     */
    int updateOrderStatusById(Map<String, Object> paramMap);

    /**
     * 根据订单id获取订单详情pic
     *
     * @param orderId
     * @return
     */
    List<String> getOrderDetailPicListByOrderId(@Param("orderId") String orderId);

    /**
     * 修改详情退款标识状态
     *
     * @return
     */
    int updateDetailRefundFlagByDetailsId(Map<String, Object> paramMap);

    /**
     * 根据订单状态、物流状态获取订单list
     *
     * @param paramMap
     * @return
     */
    List<Map<String, Object>> getOrderByStatusAndStoreNo(Map<String, Object> paramMap);

    /**
     * 创建订单运单信息
     *
     * @param paramMap
     * @return
     */
    int insertLogistics(Map<String, Object> paramMap);

    /**
     * 获取订单运单信息
     *
     * @param paramMap
     * @return
     */
    Map<String, Object> getLogistics(Map<String, Object> paramMap);

    /**
     * 保存订单收获地址
     *
     * @param orderId
     * @param contacts
     * @param mobilePhone
     * @param detailedAddress
     * @return
     */
    int saveOrderAddress(@Param("orderId") String orderId, @Param("contacts") String contacts, @Param("mobilePhone") String mobilePhone, @Param("detailedAddress") String detailedAddress);


    /**
     * 取消订单
     *
     * @param paramMap
     * @return
     */
    int cancelOrder(Map<String, Object> paramMap);

//    /**
//     * 订单状态更换
//     */
//    void autoSwitchOrderStatus();
//
//    /**
//     * 订单完成状态(已完成/确认收货)
//     */
//    void receivedOrdersStatus();

    Map getOrderStatistics(Map paramMap);

    /**
     * 获取活动订单详情
     *
     * @param paramMap
     * @return
     */
    LinkedList<OrderDetail> getActivityOrderData(Map<String, Object> paramMap);

    /**
     * 模糊查询订单列表
     *
     * @param orderId
     * @return
     */
    List<Map<String, Object>> getOrdersByFuzzyId(@Param("orderId") String orderId);

    /**
     * 修改订单交易类型
     * @param orderId
     * @param tradeType
     * @return
     */
    int updateOrderTradeType(@Param("orderId") String orderId, @Param("tradeType") String tradeType);
    
    
	/**
	 * 推广 订单明细及统计
	 * 已挚订单
	 * @param paramMap
	 * @return
	 */
	List<Map> orderDetailStatistics(Map paramMap);

	 /**
     * 生成礼包订单
     * @param paramMap
     * @return
     */
	void addGiftPackOrder(HashMap<String, Object> orderMap);

	 /**
     * 生成礼包订单详情
     * @param paramMap
     * @return
     */
	void addGiftPackOrderDetails(HashMap<String, Object> detailMap);

	/**
	 * 获取订单折扣价
	 * @param orderId
	 * @return
	 */
	BigDecimal getOrderDiscountAmount(@Param("orderId") String orderId);
}
