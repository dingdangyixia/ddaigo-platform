package com.purchase.mapper;

import com.purchase.entity.Cart;
import com.purchase.util.RequestForm;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

@MyBatisRepository
public interface CartMapper extends Mapper<Cart> {


    /**
     * 根据用户ID、门店编号获取购物车list
     *
     * @param paramMap
     * @return
     */
    List<Cart> getCartByWxUidAndStoreNo(Map<String, Object> paramMap);

    /**
     * 添加商品到购物车/修改数量
     *
     * @param paramMap
     * @return
     */
    int insertOrUpdateGoodsToCart(Map<String, Object> paramMap);

    /**
     * 修改购物车商品
     *
     * @param paramMap
     * @return
     */
    int updateOldGoodsInCart(Map<String, Object> paramMap);

    /**
     * 修改购物车商品
     *
     * @param paramMap
     * @return
     */
    int updateGoodsInCart(Map<String, Object> paramMap);

    /**
     * 根据联合主键（wxuid、门店编号、商品id、尺寸）获取购物车list
     *
     * @param paramMap
     * @return
     */
    List<Map<String, Object>> getCartListByPrimaryKeys(List<Map<String, Object>> paramMap);

    /**
     * 根据联合主键（wxuid、门店编号、商品id、尺寸）获取购物车
     *
     * @param paramMap
     * @return
     */
    Map<String, Object> getCartByPrimaryKeys(Map<String, Object> paramMap);

    Map<String, Object> getGoodsInCart(Map<String, Object> paramMap);

//	/**
//	 * 定期删除失效购物车单
//	 */
//	void cleanInvalidCarts();

    /**
     * 备份
     */
    void bakCarts(Map<String, Object> paramMap);

    /**
     * 删除
     * @param paramMap
     */
    void deleteCart(Map<String, Object> paramMap);

}
