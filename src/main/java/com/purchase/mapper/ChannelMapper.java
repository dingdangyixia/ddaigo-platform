package com.purchase.mapper;

import java.util.List;
import java.util.Map;

import com.purchase.entity.Channel;

import tk.mybatis.mapper.common.Mapper;

@MyBatisRepository
public interface ChannelMapper extends Mapper<Channel>  {
	Integer checkChannelpeople(String wxUid);
	/**
	 * 获取渠道列表
	 * @param List
	 * @return
	 */
	List<Channel> channelList(Map<String, Object> paramMap);
}
