package com.purchase.mapper;

import com.purchase.entity.BannerStore;

import tk.mybatis.mapper.common.Mapper;

@MyBatisRepository
public interface BannerStoreMapper extends Mapper<BannerStore> {

}
