package com.purchase.mapper;

import com.purchase.entity.AccountBonus;

import tk.mybatis.mapper.common.Mapper;


public interface AccountBonusMapper extends Mapper<AccountBonus> {

}