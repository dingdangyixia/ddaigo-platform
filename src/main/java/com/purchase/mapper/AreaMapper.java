package com.purchase.mapper;

import java.util.List;
import java.util.Map;

import com.purchase.entity.Area;
import com.purchase.util.RequestForm;

import tk.mybatis.mapper.common.Mapper;

@MyBatisRepository
public interface AreaMapper extends Mapper<Area>  {

//	//省
//	List<Map<String, Object>> getAllProvince();
	
	//查询地区
	List<Map<String, Object>> getAreaByPid(Map<String, Object> paramMap);
	
}
