package com.purchase.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class OrderDetail {
    private Integer activityId;
    private Integer orderGoodsNum;
    private String address;
    private String specsName;
    private String orderId;
    private String phone;
    private BigDecimal goodsPrice;
    private BigDecimal orderPrice;
    private BigDecimal originalPrice;
    private String activityName;
    private String orderStatus;
    private String logisticStatus;
    private String goodsName;
    private Integer goodsNum;
    private String contacts;
    private Date createTime;
    private BigDecimal postage;
    private String goodsCode;
    private String goodsColor;
    private String goodsPic;

}
