package com.purchase.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Table(name = "p_banner_store")
public class BannerStore {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    private String storeNo;

    private Integer bannerId;

}
