package com.purchase.entity;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 退款流水
 */
@Data
@Table(name = "p_refund_pay")
public class RefundPay {

    @Id
    @GeneratedValue(generator = "JDBC")
    private String id;

    private String refundOrderId;

    private String orderId;

    private String transactionRefundId;

    private BigDecimal totalFee;

    private BigDecimal refundTotalFee;

    private Date createTime;

    private Date updateTime;

    private Date endTime;

    private String refundRecvAccount;

    private String refundStatus;

    private String refundFailDesc;

}
