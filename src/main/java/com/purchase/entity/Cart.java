package com.purchase.entity;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 购物车
 */
@Data
@Table(name = "p_cart")
public class Cart {

    @Id
    @GeneratedValue(generator = "JDBC")
    private String wxUid;

    private String storeNo;

    private Integer goodsId;

    private String goodsName;

    private String goodsPic;

    private Integer goodsNum;

    private BigDecimal goodsPrice;

    private String goodsColor;

    private String specsName;

    private Date addTime;

    private BigDecimal originalPrice;

    private Integer activityId;

    @Transient
    private Integer soldOut;

    @Transient
    private Integer stockNum;

    @Transient
    private Integer oldGoodsId;

    @Transient
    private String oldSpecsName;

    @Transient
    private String activityName;

    @Transient
    private Date activityEndTime;

    @Transient
    private Integer activityStatus;
    
    @Transient
    private BigDecimal memberPrice;
    
    @Transient
    private BigDecimal discountPrice;
    
}