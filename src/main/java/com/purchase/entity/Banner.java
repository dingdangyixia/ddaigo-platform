package com.purchase.entity;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name = "p_banner")
public class Banner {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    private String name;

    private String url;

    private Integer type;

    private Integer action;
    
    private String link;

    private Date createTime;

    private Date updateTime;

    private Integer status;

    private Integer sort;
    
    private String platform;
}
