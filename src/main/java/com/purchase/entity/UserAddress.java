package com.purchase.entity;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

//用戶表
@Data
@Table(name = "p_user_address")
public class UserAddress {

	@Id
	@GeneratedValue(generator = "JDBC")
	
	private Integer id;
	
	private String wxUid ;
	
	private String province;
	
	private String city;
	
	private String area;
	
	private String detailedAddress;
	
	private String contacts;
	
	private String mobilePhone;
	
	private String telephone;
	
	private String alias;
	
	private String remarks;
	
	private Date createTime;
	
	
	private Integer defaultOptions;
	
	private Integer status;
}
