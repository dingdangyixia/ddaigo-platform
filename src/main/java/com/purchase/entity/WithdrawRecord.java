package com.purchase.entity;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Table(name = "p_withdraw_record")
public class WithdrawRecord {

    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;
    private String wxUid;
    private BigDecimal applyAmount;
    private String withdrawAccount;
    private Integer withdrawChannel;
    private Date applyTime;
    private Date withdrawTime;
    private Date createTime;
    private Date updateTime;
    private Integer applyStatus;
    private Integer auditorUserId;
    private String auditorUserName;

    @Transient
    private String personName;

}
