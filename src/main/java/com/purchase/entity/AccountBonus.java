package com.purchase.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "b_account_bonus")
public class AccountBonus {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer bonusId;

    private String masterUid;

    private String parentWxUid;

    private String wxUid;

    private String inviteCode;

    private Integer userStatus;

    private String incomeGrade;

    private BigDecimal incomeRate;

    private BigDecimal incomeAmount;

    private Integer incomeStatus;

    private Date incomeDate;

    private Date createTime;

    private String buyerWxUid;

    private Integer buyerStatus;

    private Integer goodsId;

    private String orderId;

    private String displayContent;

    private BigDecimal discountRate;

    private BigDecimal discountPrice;

    private BigDecimal memberRate;

    private BigDecimal memberPrice;

    private BigDecimal vipSaveAmount;

    private Integer goodsQty;

    public Integer getBonusId() {
        return bonusId;
    }

    public void setBonusId(Integer bonusId) {
        this.bonusId = bonusId;
    }

    public String getMasterUid() {
        return masterUid;
    }

    public void setMasterUid(String masterUid) {
        this.masterUid = masterUid;
    }

    public String getParentWxUid() {
        return parentWxUid;
    }

    public void setParentWxUid(String parentWxUid) {
        this.parentWxUid = parentWxUid;
    }

    public String getWxUid() {
        return wxUid;
    }

    public void setWxUid(String wxUid) {
        this.wxUid = wxUid;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public String getIncomeGrade() {
        return incomeGrade;
    }

    public void setIncomeGrade(String incomeGrade) {
        this.incomeGrade = incomeGrade;
    }

    public BigDecimal getIncomeRate() {
        return incomeRate;
    }

    public void setIncomeRate(BigDecimal incomeRate) {
        this.incomeRate = incomeRate;
    }

    public BigDecimal getIncomeAmount() {
        return incomeAmount;
    }

    public void setIncomeAmount(BigDecimal incomeAmount) {
        this.incomeAmount = incomeAmount;
    }

    public Integer getIncomeStatus() {
        return incomeStatus;
    }

    public void setIncomeStatus(Integer incomeStatus) {
        this.incomeStatus = incomeStatus;
    }

    public Date getIncomeDate() {
        return incomeDate;
    }

    public void setIncomeDate(Date incomeDate) {
        this.incomeDate = incomeDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getBuyerWxUid() {
        return buyerWxUid;
    }

    public void setBuyerWxUid(String buyerWxUid) {
        this.buyerWxUid = buyerWxUid;
    }

    public Integer getBuyerStatus() {
        return buyerStatus;
    }

    public void setBuyerStatus(Integer buyerStatus) {
        this.buyerStatus = buyerStatus;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDisplayContent() {
        return displayContent;
    }

    public void setDisplayContent(String displayContent) {
        this.displayContent = displayContent;
    }

    public BigDecimal getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(BigDecimal discountRate) {
        this.discountRate = discountRate;
    }

    public BigDecimal getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(BigDecimal discountPrice) {
        this.discountPrice = discountPrice;
    }

    public BigDecimal getMemberRate() {
        return memberRate;
    }

    public void setMemberRate(BigDecimal memberRate) {
        this.memberRate = memberRate;
    }

    public BigDecimal getMemberPrice() {
        return memberPrice;
    }

    public void setMemberPrice(BigDecimal memberPrice) {
        this.memberPrice = memberPrice;
    }

    public BigDecimal getVipSaveAmount() {
        return vipSaveAmount;
    }

    public void setVipSaveAmount(BigDecimal vipSaveAmount) {
        this.vipSaveAmount = vipSaveAmount;
    }

    public Integer getGoodsQty() {
        return goodsQty;
    }

    public void setGoodsQty(Integer goodsQty) {
        this.goodsQty = goodsQty;
    }
}