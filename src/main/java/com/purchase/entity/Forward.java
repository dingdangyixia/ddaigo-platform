package com.purchase.entity;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Table(name="p_forward")
public class Forward {
	@Id
	@GeneratedValue(generator="JDBC")
	private Integer id;
	private String wxUid;
	private Integer goodsId;
	private String goodsName;
	private String picType;
	private String forwardChannel;
	private Date createTime;
}
