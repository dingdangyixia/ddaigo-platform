package com.purchase.entity;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name = "p_invite_code")
public class InviteCode {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;
    private String wxUid;
    private String createWxUid;
    private String inviteCode;
    private Integer codeStatus;
    private Integer parentId;
    private Date createTime;
    private Date expireTime;
}
