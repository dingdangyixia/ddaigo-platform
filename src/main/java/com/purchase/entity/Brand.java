package com.purchase.entity;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Table(name = "p_brand")
public class Brand {
	@Id
	@GeneratedValue(generator = "JDBC")
    private Integer id;

	private String name;

    private String story;

    private String url;

    private Date createTime;

	
}