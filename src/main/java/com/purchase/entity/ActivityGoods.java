package com.purchase.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
//活动-商品关系表
@Data
@Table(name = "p_activity_goods")
public class ActivityGoods {
	
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;
	
	private Integer activityId;
	
	private Integer goodsId;
	
	
}
