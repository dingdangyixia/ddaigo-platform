package com.purchase.entity;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Table(name="p_invite")
public class Invite {
	@Id
	@GeneratedValue(generator="JDBC")
	private Integer id;
	private String inviteCode;
	private Date createTime;
}
