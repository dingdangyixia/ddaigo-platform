package com.purchase.service;

import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

public interface WxAppAuthorizeService {

	ResponseForm app2authorize(RequestForm param);

	ResponseForm getUserInfo(RequestForm param);

}
