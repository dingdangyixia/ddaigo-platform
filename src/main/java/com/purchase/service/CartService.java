package com.purchase.service;

import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

public interface CartService {

    /**
     * 获取购物车列表
     * @param param
     * @return
     */
    ResponseForm getCart(RequestForm param);

    /**
     * 添加商品进购物车
     * @param param
     * @return
     */
    ResponseForm insertOrUpdateGoodsToCart(RequestForm param);

    /**
     * 修改购物车商品属性
     * @param param
     * @return
     */
    ResponseForm modifyGoodsInCart(RequestForm param);

    /**
     * 删除购物车内商品
     * @param param
     * @return
     */
    ResponseForm removeGoodsInCart(RequestForm param);

    /**
     * 清空购物车
     * @param param
     * @return
     */
    ResponseForm emptyCart(RequestForm param);

}
