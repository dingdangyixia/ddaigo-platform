package com.purchase.service;

import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

public interface ChannelService {

	/**
	 * @Title: channelList   
	 * @Description: 渠道列表  
	 * @return: ResponseForm
	 */
	ResponseForm channelList(RequestForm param);
	
	/**
	 * @Title: addChannel   
	 * @Description: 新增渠道  
	 * @return: ResponseForm
	 */
	ResponseForm addChannel(RequestForm param);
	
	/**
	 * @Title: editChannel   
	 * @Description: 编辑渠道  
	 * @return: ResponseForm
	 */
	ResponseForm editChannel(RequestForm param);
	
	/**
	 * @Title: getChannel   
	 * @Description: 获取一个渠道  
	 * @return: ResponseForm
	 */
	ResponseForm getChannel(RequestForm param);

	/**
	 * @desc 检查用户是否是推广者
	 * @param param
	 * @return
	 */
	ResponseForm checkChannelpeople(RequestForm param);

	
	
}
