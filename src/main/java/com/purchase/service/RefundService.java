package com.purchase.service;


import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

import javax.servlet.http.HttpServletRequest;

public interface RefundService {

    /**
     * 申请退款
     * @param param
     * @return
     */
    ResponseForm refund(RequestForm param);

    /**
     * 接收微信退款结果通知
     * @param request
     * @return
     */
    String refundNotify(HttpServletRequest request);

    /**
     * 审核退款
     * @param param
     * @return
     */
    ResponseForm refundAudit(RequestForm param);

    /**
     * 退款查询
     * @param param
     * @return
     */
    ResponseForm refundQuery(RequestForm param);
}
