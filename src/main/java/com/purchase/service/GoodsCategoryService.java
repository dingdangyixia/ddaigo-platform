package com.purchase.service;

import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

public interface GoodsCategoryService {

	ResponseForm getGoodsCategoryByPid(RequestForm param);

	ResponseForm getGoodsCategoryCount(RequestForm param);

}
