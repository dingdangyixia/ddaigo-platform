package com.purchase.service.impl;

import com.purchase.entity.WithdrawRecord;
import com.purchase.mapper.WithdrawRecordMapper;
import com.purchase.service.WithdrawRecordService;
import com.purchase.util.LogInfo;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class WithdrawRecordServiceImpl implements WithdrawRecordService {

    @Autowired
    private WithdrawRecordMapper withdrawRecordMapper;

    @Override
    public ResponseForm addWithdrawRecord(RequestForm param) {
        ResponseForm result = new ResponseForm();
        Map<String, Object> paramMap = (Map<String, Object>) param.getData();
        String wxUid = null;
        try {
            wxUid = (String) paramMap.get("wxUid");
            if (!StringUtils.isNotBlank(wxUid)) {
                result.setStatus(false);
                result.setMessage(LogInfo.PARAM_ERROR);
                log.error(LogInfo.PARAM_ERROR);
                return result;
            }
            List<WithdrawRecord> withdrawRecords = withdrawRecordMapper.selectList(wxUid);
            boolean dateMatch = withdrawRecords.parallelStream()
                    .anyMatch(withdrawRecord -> LocalDate.now().equals(withdrawRecord.getApplyTime()
                            .toInstant().atZone(ZoneId.systemDefault()).toLocalDate()));

            if (dateMatch) {
                result.setStatus(false);
                result.setMessage("每日仅可提现一次");
                log.debug("每日仅可提现一次,wxUid=" + wxUid);
                return result;
            }
            withdrawRecordMapper.insertOne(paramMap);
        } catch (Exception e) {
            result.setStatus(false);
            result.setMessage(LogInfo.ERROR);
            log.error("创建提现申请记录error,wxUid=" + wxUid, e);
        }
        return result;
    }

    @Override
    public ResponseForm getWithdrawRecordList(RequestForm param) {
        ResponseForm result = new ResponseForm();
        Map<String, Object> paramMap = (Map<String, Object>) param.getData();
        String wxUid = null;
        try {
            wxUid = (String) paramMap.get("wxUid");
            if (!StringUtils.isNotBlank(wxUid)) {
                result.setStatus(false);
                result.setMessage(LogInfo.PARAM_ERROR);
                log.error(LogInfo.PARAM_ERROR);
                return result;
            }
            List<WithdrawRecord> withdrawRecords = withdrawRecordMapper.selectList(wxUid);
            result.setData(withdrawRecords);
        } catch (Exception e) {
            result.setStatus(false);
            result.setMessage(LogInfo.ERROR);
            log.error("查询提现申请记录列表error,wxUid=" + wxUid, e);
        }
        return result;
    }

    @Override
    public ResponseForm getWithdrawRecordListForManage(RequestForm param) {
        //todo 管理平台提现列表
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public ResponseForm approveWithdrawRecord(RequestForm param) {
        ResponseForm result = new ResponseForm();
        Map<String, Object> paramMap = (Map<String, Object>) param.getData();
        Integer id = (Integer) paramMap.get("id");
        try {
            WithdrawRecord withdrawRecord = withdrawRecordMapper.selectById(id);

            //todo 调用提现接口

            withdrawRecordMapper.updateById(paramMap);
        } catch (Exception e) {
            result.setStatus(false);
            result.setMessage(LogInfo.ERROR);
            log.error("审核提现申请error", e);
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        return result;
    }

}
