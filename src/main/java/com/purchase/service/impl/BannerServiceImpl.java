package com.purchase.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.purchase.entity.Banner;
import com.purchase.mapper.BannerMapper;
import com.purchase.service.BannerService;
import com.purchase.util.LogInfo;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;
import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.*;

@Slf4j
@Service
public class BannerServiceImpl implements BannerService {

	@Autowired
	private BannerMapper bannerMapper;

	@Override
	public ResponseForm getBrandList(RequestForm param) {
		ResponseForm result = new ResponseForm();
		try {
			List<Banner> banners = bannerMapper.selectList();
			result.setData(banners);
		} catch (Exception e) {
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error("查询轮播图列表error", e);
		}
		return result;
	}

	@Override
	public ResponseForm addBanner(RequestForm param) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			String name = (String) paramMap.get("name");
			String url = (String) paramMap.get("url");
			if (!StringUtils.isNotBlank(name) || !StringUtils.isNotBlank(url)) {
				result.setStatus(false);
				result.setMessage(LogInfo.PARAM_ERROR);
				log.error(LogInfo.PARAM_ERROR);
				return result;
			}

			bannerMapper.insertOne(paramMap);
		} catch (Exception e) {
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error("创建轮播图error", e);
		}
		return result;
	}

	@Override
	public ResponseForm editBrand(RequestForm param) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			bannerMapper.updateById(paramMap);
		} catch (Exception e) {
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error("修改轮播图error", e);
		}
		return result;
	}

	@Override
	public ResponseForm getBannerById(RequestForm param) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			Banner banner = bannerMapper.selectById(Integer.valueOf(String.valueOf(paramMap.get("id"))));
			result.setData(banner);
		} catch (Exception e) {
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error("查询轮播图error", e);
		}
		return result;
	}

	@Override
	public ResponseForm delBanner(RequestForm param) {
		ResponseForm result = new ResponseForm();
		Map<String, Object> paramMap = (Map<String, Object>) param.getData();
		try {
			String id = (String) paramMap.get("id");
			if (!StringUtils.isNotBlank(id)) {
				result.setStatus(false);
				result.setMessage(LogInfo.PARAM_ERROR);
				log.error(LogInfo.PARAM_ERROR);
				return result;
			}

			bannerMapper.delBanner(Integer.valueOf(id));
		} catch (Exception e) {
			result.setStatus(false);
			result.setMessage(LogInfo.ERROR);
			log.error("删除轮播图error", e);
		}
		return result;
	}

	@Override
	public ResponseForm getAdBanner(RequestForm param) {
		ResponseForm result = new ResponseForm();
		if (param.getData() != null) {
			Map paramMap = (Map) param.getData();
			String platform = (String) paramMap.get("tradeType");
			String limit = (String) paramMap.get("limit");
			com.purchase.util.Page.parsePage(paramMap);
			try {
				Example example = new Example(Banner.class);
				example.setOrderByClause("sort DESC");
				Criteria criteria = example.createCriteria();
				criteria.andEqualTo("type", 1);/* type:1为广告 */
				criteria.andEqualTo("status", 1);/* status:1为有效 */
				criteria.andNotEqualTo("platform", "APP".equalsIgnoreCase(platform)?"JSAPI":"APP");
				Page<Object> pageHelper = PageHelper.startPage((int) paramMap.get("page"), (int) paramMap.get("size"));
				List<Banner> adBannerList = bannerMapper.selectByExample(example);
				result.succ("操作成功", adBannerList);
			} catch (Exception e) {
				e.printStackTrace();
				result.fail("400", "请求服务器错误");
			}
		}else {
			result.fail("400", "参数为空");
		}
		return result;
	}

}
