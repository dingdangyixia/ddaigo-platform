package com.purchase.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.purchase.entity.Forward;
import com.purchase.mapper.ForwardMapper;
import com.purchase.service.ForwardService;
import com.purchase.util.MyBeanUtils;
import com.purchase.util.RequestForm;
import com.purchase.util.ResponseForm;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ForwardServiceImpl implements ForwardService {
	
	@Resource
	ForwardMapper forwardMapper;
	
	@Transactional
	@Override
	public ResponseForm saveForward(RequestForm param) {
		ResponseForm result = new ResponseForm();
		if(param.getData()!=null) {
			Map paramMap = (Map) param.getData();
			Forward forward = MyBeanUtils.populate(new Forward(), paramMap);
			try {
				forwardMapper.insertSelective(forward);
				result.setCode("200");
				result.setStatus(true);
				result.setMessage("信息保存成功");
			} catch (Exception e) {
				result.setCode("500");
				result.setMessage("信息保存失败");
				result.setStatus(false);
				log.error("[mysql] data query error");
				e.printStackTrace();
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); 
			}
		}
		return result;
	}

}
