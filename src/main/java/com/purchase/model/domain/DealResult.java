/**
 * *************************************
 * Copyright(c)2016 Dingdang Yixia 
 * All Rights Reserved.
 *
 * @File: DealResult.java
 * @Package: com.purchase.modle.domain
 * @Description:
 * 
 * @Version: V1.0
 * @Author: Yidong.Xiang
 * @Date: 2018年7月11日 下午7:42:54
 * 
 * *************************************
 */
package com.purchase.model.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: DealResult.java
 * @Module: 交易结果，待分佣对象
 * @Description: 包含结算佣金凭据
 * 
 * @author: Yidong.Xiang
 * @date: 2018年7月11日 下午7:42:54
 * 
 */
public class DealResult {

	private List<BonusVoucher> resultList;
	
	public DealResult()
	{
		resultList = new ArrayList<BonusVoucher>();
	}
	public void setResultList(List<BonusVoucher> resultList)
	{
		this.resultList = resultList;
	}
	public List<BonusVoucher> getResultList()
	{
		return this.resultList;
	}
	public int getResultSize()
	{
		return resultList.size();
	}
	public void addDealInfo(BonusVoucher bonusVoucher)
	{
		resultList.add(bonusVoucher);
	}
}
