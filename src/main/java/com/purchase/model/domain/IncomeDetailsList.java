/**
 * *************************************
 * Copyright(c)2016 Dingdang Yixia 
 * All Rights Reserved.
 *
 * @File: IncomeDetailsList.java
 * @Package: com.purchase.model.domain
 * @Description:
 * 
 * @Version: V1.0
 * @Author: Yidong.Xiang
 * @Date: 2018年7月16日 下午4:26:09
 * 
 * *************************************
 */
package com.purchase.model.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: IncomeDetailsList.java
 * @Module: 用户收益明细列表
 * @Description: 
 * 
 * @author: Yidong.Xiang
 * @date: 2018年7月16日 下午4:26:09
 * 
 */
public class IncomeDetailsList {

	private List<IncomeDetails> resultList;
	
	public IncomeDetailsList()
	{
		resultList = new ArrayList<IncomeDetails>();
	}
	public void setResultList(List<IncomeDetails> resultList)
	{
		this.resultList = resultList;
	}
	public List<IncomeDetails> getResultList()
	{
		return this.resultList;
	}
	public int getResultSize()
	{
		return resultList.size();
	}
	public void addDealInfo(IncomeDetails incomeDetails)
	{
		resultList.add(incomeDetails);
	}
}
