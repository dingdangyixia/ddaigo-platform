/**
 * *************************************
 * Copyright(c)2016 Dingdang Yixia 
 * All Rights Reserved.
 *
 * @File: VendorTeam.java
 * @Package: com.purchase.model.domain
 * @Description:
 * 
 * @Version: V1.0
 * @Author: Yidong.Xiang
 * @Date: 2018年7月17日 下午7:37:42
 * 
 * *************************************
 */
package com.purchase.model.domain;

import com.purchase.entity.Vendor;

import lombok.Data;

/**
 * @ClassName: VendorTeam.java
 * @Module: 	代购团队
 * @Description: 
 * 
 * @author: Yidong.Xiang
 * @date: 2018年7月17日 下午7:37:42
 * 
 */
@Data
public class VendorTeam {

	private Vendor vendor;		//代购
	private Vendor supVendor;	//直属代购
	private Vendor teamOfDirector;	//直属团队总监
	private Vendor teamOfSupDirector;	//直属团队总监的上级总监
	private Vendor teamOfMaster;		//直属团队总经理
	private Vendor teamOfSupMaster;		//直属团队总经理的上级总经理

	public VendorTeam(Vendor vendor) {
		this.vendor=vendor;
	}	
}
