/**
 * *************************************
 * Copyright(c)2016 Dingdang Yixia 
 * All Rights Reserved.
 *
 * @File: IncomeDetails.java
 * @Package: com.purchase.model.domain
 * @Description:
 * 
 * @Version: V1.0
 * @Author: Yidong.Xiang
 * @Date: 2018年7月16日 下午4:21:19
 * 
 * *************************************
 */
package com.purchase.model.domain;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;

/**
 * @ClassName: IncomeDetails.java
 * @Module: 用户收益记录信息
 * @Description: 
 * 
 * @author: Yidong.Xiang
 * @date: 2018年7月16日 下午4:21:19
 * 
 */
@Data
public class IncomeDetails {

	private String nickName;
	private String headImage;
	private String buyerWxUid;
	private String buyerStatus;//
	private String orderId;
	private Integer goodsId;
//	private Integer goodsQty;
//	private BigDecimal discountRate;
//	private BigDecimal discountPrice;
//	private BigDecimal memberRate;
//	private BigDecimal memberPrice;
//	private BigDecimal vipSaveAmount;
//	private String incomeGrade;
//	private BigDecimal incomeRate;
	private BigDecimal incomeAmount;
	private String incomeType;//商品、代购礼包、会员礼包、提现
	private String displayContent;//
	private String accountStatus;//已入账、未入账
	private Date accountDate;//入账日期
	private Date createDate;//交易日期
}
