/**
 * *************************************
 * Copyright(c)2016 Dingdang Yixia 
 * All Rights Reserved.
 *
 * @File: BonusAccount.java
 * @Package: com.purchase.model.domain
 * @Description:
 * 
 * @Version: V1.0
 * @Author: Yidong.Xiang
 * @Date: 2018年7月16日 下午10:46:16
 * 
 * *************************************
 */
package com.purchase.model.domain;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;

/**
 * @ClassName: BonusAccount.java
 * @Module: 佣金账户信息
 * @Description: 
 * 
 * @author: Yidong.Xiang
 * @date: 2018年7月16日 下午10:46:16
 * 
 */
@Data
public class BonusAccount {

	private Integer totalVisitor;
	private Integer totalMember;
	private Integer totalVendor;
	private Integer totalDirector;
	private Integer totalMaster;
	private BigDecimal preIncomeAmount;
	private BigDecimal canCashoutAmount;
	private BigDecimal cashoutAmount;
	private Date accountTime;
}
