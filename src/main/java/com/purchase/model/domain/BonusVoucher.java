package com.purchase.model.domain;

import java.math.BigDecimal;

import lombok.Data;
/**
 * 
 * @ClassName: BonusVoucher.java
 * @Module: 结算佣金的凭据模型
 * @Description: 
 * 
 * @author: Yidong.Xiang
 * @date: 2018年7月09日 下午9:31:10
 *
 */
@Data
public class BonusVoucher {

	private String orderId;
	private Integer goodsId;
	private BigDecimal discountRate;
	private BigDecimal discountPrice;
	private BigDecimal memberRate;
	private BigDecimal memberPrice;
	private Integer goodsQty;
	private BigDecimal vipSaveAmount;
	private String displayContent;
	private String buyerWxUid;
//	private Integer buyerStatus;
}
